new Vue({
    el: '#app',
    data: {
      currentTime: 45,
      timer: null,
    },
    mounted() {
      this.startTimer()
    },
    destroyed() {
      this.stopTimer()
    },
    methods: {
      startTimer() {
        this.timer = setInterval(() => {
          this.currentTime--
        }, 1000)
      },
      stopTimer() {
        clearTimeout(this.timer)
      },
    },
    watch: {
      currentTime(time) {
        if (time === 0) {
          this.stopTimer()
        }
      }
    },
  })