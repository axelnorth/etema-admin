<?php

public function Profile(
    $url,
    $name,
    $gender,
    $newUsername = null)
{

return $this->ig->request('accounts/profiles/')
->addPost('_uuid', $this->ig->uuid)
->addPost('_uid', $this->ig->account_id)
->addPost('_csrftoken', $this->ig->client->getToken())
->addPost('external_url', $url)
->addPost('username', $username)
->addPost('first_name', $name)
->addPost('gender', $gender)
->getResponse(new Response\UserInfoResponse());

}