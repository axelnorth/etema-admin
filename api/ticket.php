<?php


class BuyTicket extends RequestCollection
{
    public function getOnTagProductInfo(
        $productId,
        $mediaId,
        $merchantId,
        $deviceWidth = 720)
    {
        return $this->ig->request("commerce/products/{$productId}/details/")
            ->addParam('source_media_id', $mediaId)
            ->addParam('merchant_id', $merchantId)
            ->addParam('device_width', $deviceWidth)
            ->addParam('hero_carousel_enabled', false)
            ->getResponse(new Response\OnTagProductResponse());
    }

    public function getCatalogItems(
        $catalogId,
        $query = '',
        $offset = null)
    {
        if ($offset !== null) {
            if ($offset % 20 !== 0) {
                throw new \InvalidArgumentException('Offset must be multiple of 20.');
            }
            $offset = [
                'offset' => $offset,
                'tier'   => 'products.elasticsearch.thrift.atn',
            ];
        }

        $queryParams = [
            $query,
            $catalogId,
            '96',
            '20',
            json_encode($offset),
        ];

        return $this->ig->request('js/../')
            ->addUnsignedPost('doc_id', '1747750168640998')
            ->addUnsignedPost('vc_policy', 'default')
            ->addUnsignedPost('strip_nulls', true)
            ->addUnsignedPost('strip_defaults', true)
            ->addUnsignedPost('query_params', json_encode($queryParams, JSON_FORCE_OBJECT))
            ->getResponse(new Response\GraphqlResponse());
    }

}